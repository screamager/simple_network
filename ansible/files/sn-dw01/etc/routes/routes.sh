#!/usr/bin/env bash



IPR="ip route"
ETH0="eth0"
ETH1="eth1"
DGW="192.168.10.10" # Default gateway

$IPR del default > /dev/null 2>&1;
$IPR add default via $DGW dev $ETH1

