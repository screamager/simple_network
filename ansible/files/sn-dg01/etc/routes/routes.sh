#!/usr/bin/env bash



IPR="ip route"
DEFAULT_IFACE="eth3"

$IPR del default > /dev/null 2>&1;
#Reload DHCP client for get default gateway
DHCPC="/sbin/dhclient"
CMD_TAIL="-4 -v -pf /run/dhclient.$DEFAULT_IFACE.pid -lf /var/lib/dhcp/dhclient.$DEFAULT_IFACE.leases -I -df /var/lib/dhcp/dhclient6.$DEFAULT_IFACE.leases $DEFAULT_IFACE"

$DHCPC -r $CMD_TAIL
for i in `ps aux | grep dhclient | grep $DEFAULT_IFACE | awk '{print $2}'|tr '\n' ' '`; do kill $i; done
$DHCPC $CMD_TAIL
