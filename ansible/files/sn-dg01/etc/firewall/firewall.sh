#!/usr/bin/env bash

###########################################################################
# 1. Configuration options.
# 1.1 Internet Configuration.
# 1.1.1 Static
INET0_IP="192.168.11.10"
INET0_IFACE="eth2"
INET0_BROADCAST="192.168.11.255"

# 1.1.2 TAP/TUN
#TAP0_IP="10.5.0.1"
#TAP0_IFACE="tap0"
#TAP0_IP_RANGE="10.5.0.0/24"

# 1.1.3 DHCP
INET1_IFACE="eth3"
DHCP="yes"

# 1.2 Local Area Network configuration.
LAN_IP="192.168.10.10"
LAN_IP_RANGE="192.168.10.0/24"
LAN_IFACE="eth1"

# 1.3 DMZ Configuration.
# 1.4 Localhost Configuration.
LO_IFACE="lo"
LO_IP="127.0.0.1"

# 1.5 IPTables Configuration.
IPTABLES="/sbin/iptables"

# 1.6 Other Configuration.
WEB_SERV='192.168.10.20'

LOG_LEVEL="7" # 7 - debug

###########################################################################
# 2. Module loading.
/sbin/depmod -a

# 2.1 Required modules
# 2.2 Non-Required modules

###########################################################################
# 3. /proc set up.
# 3.1 Required proc configuration
echo "1" > /proc/sys/net/ipv4/ip_forward

# 3.2 Non-Required proc confi2guration
#echo "1" > /proc/sys/net/ipv4/conf/all/rp_filter
#echo "1" > /proc/sys/net/ipv4/conf/all/proxy_arp
#echo "1" > /proc/sys/net/ipv4/ip_dynaddr

###########################################################################
# 4. rules set up.
# 4.1 Clear all rules
$IPTABLES -F
$IPTABLES -X
$IPTABLES -t nat -F
$IPTABLES -t nat -X
$IPTABLES -t mangle -F
$IPTABLES -t mangle -X

# 4.2 Filter table
# 4.2.1 Set policies
$IPTABLES -P INPUT DROP
$IPTABLES -P OUTPUT DROP
$IPTABLES -P FORWARD DROP

# 4.2.2 Create userspecified chains
## Create chain for bad tcp packets
$IPTABLES -N bad_tcp_packets

## Create separate chains for ICMP, TCP and UDP to traverse
$IPTABLES -N allowed
$IPTABLES -N tcp_packets
$IPTABLES -N udp_packets
$IPTABLES -N icmp_packets

# 4.2.3 Create content in userspecified chains
## bad_tcp_packets chain
$IPTABLES -A bad_tcp_packets -p TCP --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j REJECT --reject-with tcp-reset
$IPTABLES -A bad_tcp_packets -p TCP ! --syn -m state --state NEW -j LOG --log-prefix "New not syn:"
$IPTABLES -A bad_tcp_packets -p TCP ! --syn -m state --state NEW -j DROP

## allowed chain
$IPTABLES -A allowed -p TCP --syn -j ACCEPT
$IPTABLES -A allowed -p TCP -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A allowed -p TCP -j DROP

## TCP rules
#$IPTABLES -A tcp_packets -p TCP -s 0/0 --dport 21 -j allowed
$IPTABLES -A tcp_packets -p TCP -s 0/0 --dport 22 -j allowed
$IPTABLES -A tcp_packets -p TCP -s 0/0 --dport 80 -j allowed
$IPTABLES -A tcp_packets -p TCP -s 0/0 --dport 113 -j allowed

## UDP ports

$IPTABLES -A udp_packets -p UDP -s 0/0 --source-port 53 -j ACCEPT
if [[ $DHCP == "yes" ]] ; then
    $IPTABLES -A udp_packets -i $INET1_IFACE -p UDP --sport 67 --dport 68 -j ACCEPT
fi

$IPTABLES -A udp_packets -p UDP -s 0/0 --destination-port 53 -j ACCEPT
$IPTABLES -A udp_packets -p UDP -s 0/0 --destination-port 123 -j ACCEPT
$IPTABLES -A udp_packets -p UDP -s 0/0 --destination-port 2074 -j ACCEPT
$IPTABLES -A udp_packets -p UDP -s 0/0 --destination-port 4000 -j ACCEPT

## ICMP rules
$IPTABLES -A icmp_packets -p ICMP -s 0/0 --icmp-type 8 -j ACCEPT
$IPTABLES -A icmp_packets -p ICMP -s 0/0 --icmp-type 11 -j ACCEPT

# 4.2.4 INPUT chain
## Bad TCP packets we don't want.
$IPTABLES -A INPUT -p TCP -j bad_tcp_packets

## Rules for special networks not part of the Internet
$IPTABLES -A INPUT -p ALL -i $LAN_IFACE -s $LAN_IP_RANGE -j ACCEPT
$IPTABLES -A INPUT -p ALL -i $LO_IFACE -s $LO_IP -j ACCEPT

## Special rule for DHCP requests from LAN, which are not caught properly otherwise.
$IPTABLES -A INPUT -p UDP -i $LAN_IFACE --dport 67 --sport 68 -j ACCEPT

## Rules for incoming packets from the internet.
$IPTABLES -A INPUT -p ALL -d $INET0_IP -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A INPUT -p TCP -i $INET0_IFACE -j tcp_packets
$IPTABLES -A INPUT -p UDP -i $INET0_IFACE -j udp_packets
$IPTABLES -A INPUT -p ICMP -i $INET0_IFACE -j icmp_packets

$IPTABLES -A INPUT -p ALL -i $INET1_IFACE -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPTABLES -A INPUT -p TCP -i $INET1_IFACE -j tcp_packets
$IPTABLES -A INPUT -p UDP -i $INET1_IFACE -j udp_packets
$IPTABLES -A INPUT -p ICMP -i $INET1_IFACE -j icmp_packets

## If you have a Microsoft Network on the outside of your firewall, you may
## also get flooded by Multicasts. We drop them so we do not get flooded by logs
#$IPTABLES -A INPUT -i $INET0_IFACE -d 224.0.0.0/8 -j DROP

## Log weird packets that don't match the above.
$IPTABLES -A INPUT -m limit --limit 3/minute --limit-burst 3 -j LOG --log-level $LOG_LEVEL --log-prefix "IPT INPUT packet died: "

# 4.2.5 FORWARD chain
## Bad TCP packets we don't want
$IPTABLES -A FORWARD -p TCP -j bad_tcp_packets

## Accept the packets we actually want to forward
$IPTABLES -A FORWARD -i $LAN_IFACE -j ACCEPT
$IPTABLES -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

## Log weird packets that don't match the above.
$IPTABLES -A FORWARD -m limit --limit 3/minute --limit-burst 3 -j LOG --log-level $LOG_LEVEL --log-prefix "IPT FORWARD packet died: "

# HTTP server
$IPTABLES -A FORWARD -p TCP -i $INET0_IFACE -o $LAN_IFACE -d $WEB_SERV --dport 80 -j allowed

# 4.2.6 OUTPUT chain
## Bad TCP packets we don't want.
$IPTABLES -A OUTPUT -p TCP -j bad_tcp_packets

## Special OUTPUT rules to decide which IP's to allow.
$IPTABLES -A OUTPUT -p ALL -s $LO_IP -j ACCEPT
$IPTABLES -A OUTPUT -p ALL -s $LAN_IP -j ACCEPT
$IPTABLES -A OUTPUT -p ALL -s $INET0_IP -j ACCEPT
$IPTABLES -A OUTPUT -p ALL -o $INET1_IFACE -j ACCEPT

## Log weird packets that don't match the above.
$IPTABLES -A OUTPUT -m limit --limit 3/minute --limit-burst 3 -j LOG --log-level $LOG_LEVEL --log-prefix "IPT OUTPUT packet died: "

# 4.3 nat table
# 4.3.1 Set policies
# 4.3.2 Create user specified chains
# 4.3.3 Create content in user specified chains
# 4.3.4 PREROUTING chain
$IPTABLES -t nat -A PREROUTING -p TCP -i $INET0_IFACE -d $INET0_IP --dport 80 -j DNAT --to-destination $WEB_SERV
$IPTABLES -t nat -A PREROUTING -p TCP -i $LAN_IFACE -d $INET0_IP --dport 80 -j DNAT --to-destination $WEB_SERV

# 4.3.5 POSTROUTING chain
## Enable simple IP Forwarding and Network Address Translation
$IPTABLES -t nat -A POSTROUTING -o $LAN_IFACE -p TCP --dst $WEB_SERV --dport 80 -j SNAT --to-source $LAN_IP
$IPTABLES -t nat -A POSTROUTING -o $INET0_IFACE -j SNAT --to-source $INET0_IP
$IPTABLES -t nat -A POSTROUTING -o $INET1_IFACE -j MASQUERADE

# 4.3.6 OUTPUT chain
$IPTABLES -t nat -A OUTPUT -p TCP --dst $INET0_IP --dport 80 -j DNAT --to-destination $WEB_SERV

# 4.4 mangle table
# 4.4.1 Set policies
# 4.4.2 Create user specified chains
# 4.4.3 Create content in user specified chains
# 4.4.4 PREROUTING chain
# 4.4.5 INPUT chain
# 4.4.6 FORWARD chain
# 4.4.7 OUTPUT chain
# 4.4.8 POSTROUTING chain

###########################################################################
iptables-save > /etc/firewall/firewall-save.rules
chmod u=rwx,g=,o= /etc/firewall/firewall-save.rules
