#!/usr/bin/env bash

LOCAL_DIR=`dirname $0`;

# Create temp file with password
echo -n "Enter the vault password: "
read -s PASSWD
PASSWD_DIR=`mktemp -d -p "$LOCAL_DIR"`
mount -t tmpfs -o size=1M,mode=0700 tmpfs $PASSWD_DIR
echo $PASSWD > $PASSWD_DIR/vault_pass.txt

# Vars for ansible
ANSIBLE_CONFIG="$LOCAL_DIR/ansible.cfg"
PLAYBOOKS_DIR="$LOCAL_DIR/playbooks"
#CMD_TAIL=" -i $LOCAL_DIR/inventory.yml --ask-vault-pass --extra-vars @$LOCAL_DIR/passwords.yml"
CMD_TAIL=" -i $LOCAL_DIR/inventory.yml --vault-password-file $PASSWD_DIR/vault_pass.txt --extra-vars @$LOCAL_DIR/passwords.yml"

# Set password for root on all hosts
ansible-playbook $PLAYBOOKS_DIR/playbook_passwd_root.yml               $CMD_TAIL
#ansible-playbook $PLAYBOOKS_DIR/playbook_passwd_users.yml              $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/playbook_install_curl_tcpdump_nmap.yml $CMD_TAIL


# Configuring provider №1
ansible-playbook $PLAYBOOKS_DIR/sn-dp01/playbook_firewall.yml          $CMD_TAIL

# Configuring provider №2
ansible-playbook $PLAYBOOKS_DIR/sn-dp02/playbook_dhcp_server.yml       $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/sn-dp02/playbook_firewall.yml          $CMD_TAIL

# Configuring web server
ansible-playbook $PLAYBOOKS_DIR/sn-dw01/playbook_docker_install.yml    $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/sn-dw01/playbook_docker_prepare.yml    $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/sn-dw01/playbook_nginx_prepare.yml     $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/sn-dw01/playbook_routes.yml            $CMD_TAIL

# Configuring gate server
ansible-playbook $PLAYBOOKS_DIR/sn-dg01/playbook_dhcp_iface.yml        $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/sn-dg01/playbook_firewall.yml          $CMD_TAIL
ansible-playbook $PLAYBOOKS_DIR/sn-dg01/playbook_routes.yml            $CMD_TAIL

# Configuring client (workstation)
ansible-playbook $PLAYBOOKS_DIR/sn-dc01/playbook_routes.yml            $CMD_TAIL

# Delete temp file and dir with password
umount $PASSWD_DIR
rm -r $PASSWD_DIR
