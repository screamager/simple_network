#!/usr/bin/env python3

import yaml, os, sys

class ParserYAML ():
    '''
    This is parser YAML file and translete to dictionary.
    '''
    def __init__ (self, yaml_file):
        self.yaml_file = yaml_file
    def parsYAML (self):
        '''
        It's return dictionary from YAML.
        '''
        self.dict_from_yaml = {}
        with open(self.yaml_file, 'r') as f:
            try:
                self.dict_from_yaml = yaml.load(f)
            except yaml.YAMLError as err:
                print(err)
        return self.dict_from_yaml

if __name__ == '__main__':
    # This is dir where located this script
    local_dir = os.path.dirname(os.path.realpath(__file__))
    # Open inventory file
    obj_conf_yml = ParserYAML(local_dir + '/inventory.yml')
    # Create from yaml file dictionary
    dict_conf = obj_conf_yml.parsYAML()
    # Get host name from argument
    host_name = sys.argv[1]
    if host_name in dict_conf['all']['hosts'].keys():
        ssh_key = dict_conf['all']['hosts'][host_name]['ansible_private_key_file'].\
                         replace('{{inventory_hostname}}', host_name).\
                         replace('{{inventory_dir}}', local_dir)
        # Connect over ssh
        os.system('ssh -i "' + ssh_key + '" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no '
                         + dict_conf['all']['hosts'][host_name]['ansible_user'] + '@'
                         + dict_conf['all']['hosts'][host_name]['ansible_ssh_host'])
