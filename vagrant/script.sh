#!/usr/bin/env bash

LOCAL_DIR=`dirname $0`;

cd $LOCAL_DIR/sn-dc01; ./build.sh; cd ..
cd           sn-dg01; ./build.sh; cd ..
cd           sn-dw01; ./build.sh; cd ..
cd           sn-dp01; ./build.sh; cd ..
cd           sn-dp02; ./build.sh; cd ..
