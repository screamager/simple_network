#!/usr/bin/env bash

VAGRANT_HOST=$1
#Cut path to vagrant init dir
DIR_CONFIG_HOST=`vagrant global-status | grep $VAGRANT_HOST | sed 's/.* \//\//' | sed 's/ //'`
#Path to key
PATH_KEY=$DIR_CONFIG_HOST/.vagrant/machines/$VAGRANT_HOST/virtualbox/private_key
#Cut IP from "Vagrantfile"
HOST_IP=`cat $DIR_CONFIG_HOST/Vagrantfile |grep config.vm.network |sed -n 's/.*"\([0-9.]\+\).*/\1/;1p'`


ssh -i $PATH_KEY \
    -o "UserKnownHostsFile=/dev/null" \
    -o "StrictHostKeyChecking=no" \
    vagrant@$HOST_IP
