#!/usr/bin/env bash

#Cut path to vagrant init dir
HOST_ID=`vagrant global-status | grep running | sed 's/ .*$//'`
#echo $HOST_ID
for ID in echo $HOST_ID; do
    sudo vagrant halt -f $ID
done
