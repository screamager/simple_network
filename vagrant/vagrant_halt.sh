#!/usr/bin/env bash

VAGRANT_HOST=$1
#Cut path to vagrant init dir
HOST_ID=`vagrant global-status | grep $VAGRANT_HOST | sed 's/ .*$//'`
#echo $HOST_ID
sudo vagrant halt -f $HOST_ID