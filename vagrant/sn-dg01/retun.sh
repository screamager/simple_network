#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

function replace(){
    local OLD_VALUE=$1
    local NEW_VALUE=$2
    local FILE=$3

    sed -i "s/$OLD_VALUE/$NEW_VALUE/g" $FILE
}

replace $HOSTNAME  HOSTNAME  Vagrantfile
replace $CUSTOM_IP CUSTOM_IP Vagrantfile
replace $INET0_IP  INET0_IP  Vagrantfile
replace $INET1_IP  INET1_IP  Vagrantfile
