# simple_network

This is example configure simple network based on Vagrant(VirtualBox) via Ansible.

How use:

```
$ git clone https://gitlab.com/screamager/simple_network.git
$ cd simple_network
$ vagrant/script.sh # WARNING! It's will create five VirtualBox VMs total size about 10Gb
$ echo -e "passwd_root: <your password>\npasswd_old_root: <your password>\npasswd_vagrant: vagrant\n" \
    > ansible/passwords.yml # Password for users on remote hosts
$ ansible-vault encrypt ansible/passwords.yml # Enter your password for encrypt file
$ ansible/script.sh # Will be ask password for file "passwords.yml"
```

If all goes well, then will be created configured hosts:

sn-dp01:

IP - 192.168.11.11. Provider number 1, giving static IP, on which is available HTTP server.


sn-dp02:

IP - 192.168.12.11. Provider number 2, giving dinamic IP(DHCP), internet is distributed through it.


sn-dw01:

IP - 192.168.10.20. HTTP server started into the docker container based on Alpine OS.
There's two "Alpine" conrainers. One is for build Nginx from source, another for run Nginx.
Located into local network. Default gateway 192.168.10.10.


sn-dg01:

IP - 192.168.10.10 - Local net.
IP - 192.168.11.10 - Static IP for forwarding to the HTTP server.
And dinamic IP is for distributing internet into local network.
Firewall configured as script with iptables rules.


sn-dc01:

IP - 192.168.10.11 - Host emulate workstation. Located into local network.
Default gateway 192.168.10.10.


Testing:

```
$ ansible/ssh_connect.py sn-dc01
sn-dc01$ traceroute 8.8.8.8 # Test internet through sn-dp02
sn-dc01$ curl 192.168.11.10 # Test is availabeling HTTP server through static internet IP

$ ansible/ssh_connect.py sn-dp01
sn-dp01$ curl 192.168.11.10 # Test is availabeling HTTP server from external net

$ ansible/ssh_connect.py sn-dw01
sn-dw01$ traceroute 8.8.8.8 # Test internet through sn-dp02
sn-dw01$ curl 192.168.11.10 # Test is availabeling HTTP server through static internet IP

$ ansible/ssh_connect.py sn-dg01
sn-dw01$ curl 192.168.11.10 # Test is availabeling HTTP server through static internet IP
```
